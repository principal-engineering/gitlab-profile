<img align="right" src="asset/principal-engineering-logo.png" width="250" alt="Principal Engineering"/>

# Principal Engineering

## Links

- Youtube: https://www.youtube.com/@principal-engineering
- Telegram: https://t.me/principalengineering
- Blog: https://principal-engineering.ru/
- Boosty: https://boosty.to/principal-engineering

## Projects

- `development-platform`: https://gitlab.com/principal-engineering/development-platform — Development Platform. Related streams:
    - https://www.youtube.com/live/htMFKbFYBHM?si=2sxxG2FQig_NkBjU
- `intellij`:
  - `gradle-showcase`: https://gitlab.com/principal-engineering/intellij/gradle-showcase — How to use gradle. Related streams:
      - https://www.youtube.com/live/5GiiGcjCc3I?feature=share
  - `idea-plugin-showcase`: https://gitlab.com/principal-engineering/intellij/idea-plugin-showcase — Example IntelliJ.
    Idea plugins. Related streams:
      - https://www.youtube.com/live/5GiiGcjCc3I?feature=share
- `tooling`:
  - TBD
- `dbaas`: https://gitlab.com/principal-engineering/dbaas — DBaaS for internal usage. Related streams:
    - https://www.youtube.com/live/OBthXcDyyco?feature=share
    - https://www.youtube.com/live/E89UTPGSe80?feature=share
- `workqueue`: https://gitlab.com/principal-engineering/workqueue — local service workqueue built on top of postgresql
  features. Related streams:
    - https://www.youtube.com/live/04MXZilZaVs?feature=share
    - https://www.youtube.com/live/0WSF0y2N6ZE?feature=share
    - https://www.youtube.com/live/hmBJuwR6wwk?feature=share

---

💪 In code we trust!
